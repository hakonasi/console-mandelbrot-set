# Console Mandelbrot Set in C++

![Image of Mandlebrot set in a terminal](/vid/set.gif)

## Compilation

```bash
g++ -fopenmp -lncurses main.cpp -O2 -o mandelbrot_set
``` 
You can also specify number of threads to use while rendering with 

```export OMP_NUM_THREADS=<number of threads>```

For example:

```bash
export OMP_NUM_THREADS=4
``` 
## Usage
- Use WASD keys for moving
- Q and E for zooming
