#include <omp.h>
#include <vector>
#include <cstdlib>
#include <ncurses.h>
#include <chrono>

#define MAX_VALUE 100
#define ITERATIONS 500

int SCR_WIDTH, SCR_HEIGHT;

double up = 1.0;
double left = -1.5;
double rect_width = 2.0;
double rect_height = 2.0;

double zoom = 1.0f;
double x_offset = 0.0f;
double y_offset = 0.0f;

const double OFFSET_FACTOR = 0.02;
const double ZOOM_FACTOR = 0.02;

float fps_counter = 0;

std::vector<int> map;

char shading[] = " .:-=+*#%@";

double aspect_ratio;

bool is_app = true;

void checkPoint(double x, double y, int x_index, int y_index){
    double real = 0.0;
    double imag = 0.0;
    
    int i  = 0;
    for(; i < ITERATIONS; i++){
        double new_real = real*real - imag*imag + x;
        double new_imag = 2.0*imag*real + y;
        if(new_real > MAX_VALUE || new_imag > MAX_VALUE){ // checking if the values going to 'infinity' (declared as MAX_VALUE)
            break;
        }
        real = new_real;
        imag = new_imag;
    }
    int shading_index = i / (ITERATIONS / 9);
    if(shading_index < 0)
        shading_index = 0;
    else if(shading_index > 9)
        shading_index = static_cast<char>(9);
    map[y_index*SCR_WIDTH+x_index] = shading_index;
}

void render(){
	erase();
	
	for(int y = 0; y < SCR_HEIGHT; y++){
        for(int x = 0; x < SCR_WIDTH; x++){
			mvaddch(y, x, shading[map[y*SCR_WIDTH+x]]);
		}
	}
    mvprintw(0, 0, "FPS: %.1f", fps_counter);
    mvprintw(1, 0, "Iterations: %d", ITERATIONS);
	
	refresh();
}

void calculate_mandelbrot(){
    double dx = rect_width * (1.0 - 1.0/zoom);
    double dy = rect_height * (1.0 - 1.0/zoom);

    double px = 0.5;
    double py = 0.5;

    double d_left = px * dx;
    double d_up = -py * dy;

    left += d_left + (rect_width/(1.0/x_offset));
    up += d_up + (rect_height/(1.0/y_offset));
    rect_width -= dx;
    rect_height -= dy;

	double x_step = (rect_width)/static_cast<double>(SCR_WIDTH);
	double y_step = (rect_height)/static_cast<double>(SCR_HEIGHT);

    #pragma omp parallel for
	for(int y = 0; y < SCR_HEIGHT; y++){
		for(int x = 0; x < SCR_WIDTH; x++){
			double x_coord = left + x_step*x; 	// from left to right
			double y_coord = up - y_step*y; 	// from up to down
            checkPoint(x_coord, y_coord, x, y);
		}
	}
}

void checkInput(){
    zoom = 1.0;
    x_offset = y_offset = 0.0;
    int c = getch();
    switch(c){
        case 'e':
            zoom+=ZOOM_FACTOR;
            break;
        case 'q':
            zoom-=ZOOM_FACTOR;
            break;
        case 'w':
            y_offset += OFFSET_FACTOR;
            break;
        case 's':
            y_offset -= OFFSET_FACTOR;
            break;
        case 'a':
            x_offset -= OFFSET_FACTOR;
            break;
        case 'd':
            x_offset += OFFSET_FACTOR;
            break;
        case 'z':
            is_app = false;
            break;
    }
}

int main(){
	initscr();
	curs_set(0);
	noecho();
	getmaxyx(stdscr, SCR_HEIGHT, SCR_WIDTH);
	
	map.resize(SCR_WIDTH*SCR_HEIGHT, '.');

	while(is_app){
        auto start = std::chrono::high_resolution_clock::now();

        std::fill(map.begin(), map.end(), ' ');
		calculate_mandelbrot();
        auto end = std::chrono::high_resolution_clock::now();

        fps_counter = 1000.0f/std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();

		render();
        checkInput();
	}
	
	endwin();
	return 0;
}
